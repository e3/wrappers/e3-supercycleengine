#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time

import epics

# Config
sleep_dly = 1
P = "TD-M:Ctrl-SCE-1:"

# Test Handler
Test_pv = epics.PV(P + "Test-SP")

# Body
#PBMod_arr = [0,1,10,20,30,40,50,60,70,80]
#PBDest_arr = [0,10,20,30,40,50]
PBMod_arr = ["NoBeam","Conditioning","Probe","FastCommissioning","RfTest","StabilityTest","SlowCommissioning","FastTuning","SlowTuning"]
PBDest_arr = ["ISrc","LEBT","MEBT","DTL2","DTL4"]

PBMod_pv = epics.PV(P + "PBMod-Sel")
PBDest_pv = epics.PV(P + "PBDest-Sel")

print(PBMod_pv.get())
print(PBDest_pv.get())

def pv_rotate(pv,arr,it):
  if (it>=len(arr)):
    it=0
  pv.put(str(arr[it]))
  print(arr[it], end=' ')
  it+=1
  return it

def test1():
  PBMod_it = 0
  PBDest_it=0
  while (True):
    Test_pv.put(os.path.basename(__file__))
    PBMod_it=pv_rotate(PBMod_pv, PBMod_arr, PBMod_it)
    time.sleep(sleep_dly)
    PBDest_it=pv_rotate(PBDest_pv, PBDest_arr, PBDest_it)
    time.sleep(sleep_dly)
    print("")

if __name__ == '__main__':
    test1()
