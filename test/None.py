import epics

# Config
sleep_dly = 1
P = "TD-M:Ctrl-SCE-1:"

# Test Handler
Test_pv = epics.PV(P + "Test-SP")
Test_pv.put("None")
