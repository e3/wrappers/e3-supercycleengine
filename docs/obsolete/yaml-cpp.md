# yaml-cpp vendor library

The supercycle engine requires a newer version of `yaml-cpp` (0.6.3) than is included in Centos7
(0.5.1). As such, we have opted to build `yaml-cpp` from source and include it as a vendor library
in the e3 wrapper.

## Build instructions for yaml-cpp

If you want to build a new version of `yaml-cpp`, follow the instructions that are included in the
[yaml-cpp git repository](https://github.com/jbeder/yaml-cpp). Provided below is
a customized version for the purposes of updating the e3 wrapper.

### Build `yaml-cpp`

In order to build `yaml-cpp`, you will need `CMake` installed. `yaml-cpp` requires a newer version of
`CMake` than the default that comes with CentOS7. This can be done with
```bash
$ sudo yum install cmake3
```

1. Clone the `yaml-cpp` repository and checkout the correct version
   ```bash
   $ git clone https://github.com/jbeder/yaml-cpp.git
   $ cd yaml-cpp
   $ git checkout tags/yaml-cpp-0.6.3
   ```
2. Set up the build directory, run `CMake`, and the build `yaml-cpp`
   ```bash
   $ mkdir build
   $ cd build
   $ cmake3 -DYAML_BUILD_SHARED_LIBS=ON -DYAML_CPP_BUILD_TESTS=OFF ..
   $ make
   ```
3. Copy all of the built libraries and header files to the e3-supercycle engine repository. First,
   determine the path of the `e3-supercycleengine` repository. From the `yaml-cpp` main directory,
   do the following.
   ```bash
   $ mkdir -p ${e3-supercycleengine-path}/vendor/lib/linux-x86_64
   $ cp -r include ${e3-supercycleengine-path}/vendor
   $ cp build/libyaml-cpp.so* ${e3-supercycleengine-path}/vendor/lib/linux-x86_64
   ```
4. Commit your changes. Switch to the `e3-supercycleengine` repository, and run the following.
   ```bash
   $ git checkout -b yaml_update
   $ git add vendor
   $ git commit -m "Updated yaml-cpp to (new version)"
   $ git push -u origin yaml_update
   ```
5. Merge the changes into the master branch
