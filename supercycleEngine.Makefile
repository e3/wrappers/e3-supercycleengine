# @SPDX-License-Identifier LGPL-2.1-only
# @author Jerzy Jamroz (jerzy.jamroz@ess.eu)

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=supercycleEngineApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src
DEPLIB:=$(where_am_I)/$(APP)/dep/lib
DEPINCLUDE:=$(where_am_I)/$(APP)/dep/include

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

USR_CXXFLAGS += -g3 -Wall -Wextra -Wno-deprecated-declarations -Wfatal-errors -fdiagnostics-color -Wno-missing-field-initializers

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*/*.template)

# SRC libraries
# plugs
SOURCES += $(APPSRC)/plug/dlog.cpp
SOURCES += $(APPSRC)/plug/dperf.cpp
SOURCES += $(APPSRC)/support/funcs.cpp
# handlers
SOURCES += $(APPSRC)/obj/dbuf.cpp
SOURCES += $(APPSRC)/obj/seq.cpp
SOURCES += $(APPSRC)/obj/csv.cpp
# sce
SOURCES += $(APPSRC)/sce/reg.cpp
SOURCES += $(APPSRC)/sce/var.cpp
SOURCES += $(APPSRC)/sce/cycle.cpp
SOURCES += $(APPSRC)/sce/devEngine.cpp
SOURCES += $(APPSRC)/sce/interface.cpp
# devs
SOURCES += $(APPSRC)/dev/dbSupport.cpp
SOURCES += $(APPSRC)/dev/cmdOut.cpp
SOURCES += $(APPSRC)/dev/devASubBuf.cpp
SOURCES += $(APPSRC)/dev/devCmnOut.cpp
SOURCES += $(APPSRC)/dev/devCmnInp.cpp
# Env last

# Deploy build versions
HEADERS += $(APPSRC)/version.h

ifeq ($(strip $(PERL)),)
PERL = perl -CSD
endif

prebuild: version_header_file

.PHONY: version_header_file
version_header_file:
	$(PERL) -I$(EPICS_BASE)/lib/perl $(where_am_I)$(APPSRC)/genVersionHeader.pl -t "" -V $(E3_MODULE_VERSION) -N SCE_VERSION $(where_am_I)$(APPSRC)/version.h

DBDS += $(APPSRC)/dev/devs.dbd

USR_LDFLAGS += -Wl,--whole-archive $(DEPLIB)/libyaml-cpp.a -Wl,--no-whole-archive
## SYSTEM LIBS
##

USR_INCLUDES += -I$(DEPINCLUDE)

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

## This RULE should be used in case of inflating DB files
## db rule is the default in RULES_DB, so add the empty one
## Please look at e3-mrfioc2 for example.

#
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I $(APPDB)/evt
USR_DBFLAGS += -I $(APPDB)/Cmn
#
SUBS=$(wildcard $(APPDB)/*.substitutions)
SUBS+=$(wildcard $(APPDB)/*/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)
TMPS+=$(wildcard $(APPDB)/*/*.template)

vlibs:

.PHONY: vlibs
