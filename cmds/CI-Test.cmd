#!/usr/bin/env iocsh.bash
require "supercycleengine"

epicsEnvSet "TOP" "$(E3_CMD_TOP)/.."
epicsEnvSet "PSCE" "CI-Test:Ctrl-SCE-1"
epicsEnvSet "PEVG" "CI-Test:Ctrl-EVG-1"

# Load record instances
dbLoadRecords "$(TOP)/db/devg.db" "P=$(PEVG)"
iocshLoad     "$(supercycleengine_DIR)/sce.iocsh"   "P=$(PSCE), PG=$(PEVG)"

var "DLogLvl" "3"

iocInit
