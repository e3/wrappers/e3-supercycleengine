#!/usr/bin/env iocsh

# Generic EVG template
# ====================

require "mrfioc2"
require "supercycleengine"

# Config
epicsEnvSet "PEVG" "$(PEVG=TESTEVG)"
epicsEnvSet "PSCE" "$(PSCE=TESTSCE)"

# Common config
epicsEnvSet "DEBUG" ""

# EVG
iocshLoad "$(mrfioc2_DIR)/evgEss.iocsh"          "P=$(PEVG):,OBJ=EVG,PCIID=0e:00.0,FRF=352.21,FDIV=4,FEVT=88.0525,INTRF=#,EXTRF=,INTPPS=#,EXTPPS="

# SCE
iocshLoad "$(supercycleengine_DIR)/sceEss.iocsh" "P=$(PSCE):,PG=$(PEVG):,CPRD=71428"
