require "mrfioc2"
require "supercycleengine" "master"

epicsEnvSet "TOP" "$(E3_CMD_TOP)/.."
epicsEnvSet "PSCE" "TDT-M:Ctrl-SCE-1"
epicsEnvSet "PEVG" "TDT-M:Ctrl-EVG-1"
#epicsEnvSet "CPRD" "71428"
epicsEnvSet "CPRD" "1000000"
epicsEnvSet "DbufSentOffset" "50000"
epicsEnvSet "IOCNAME" "TDT-M:SC-IOC-1"
epicsEnvSet "ASG_SUBS" "$(ASG_SUBS=LOCALHOST=10.0.2.15)"

#######epicsEnvSet("autosave_DIR", "/tmp/")
#iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=/tmp,IOCNAME=EVG")

# Load record instances
iocshLoad "$(mrfioc2_DIR)/envEss.iocsh"
dbLoadRecords "$(TOP)/test/db/devg.db" "P=$(PEVG):"
# iocshLoad     "$(supercycleengine_DIR)/sce.iocsh"   "P=$(PSCE), PG=$(PEVG)"
# iocshLoad     "$(TOP)/iocsh/sce.iocsh"   "P=$(PSCE),PG=$(PEVG),CPRD=$(CPRD)"

# Common
iocshLoad "$(mrfioc2_DIR)/cmnEss.iocsh" # Redundant

iocshLoad "$(TOP)/iocsh/sceEss.iocsh" "P=$(PSCE):,PG=$(PEVG):,CPRD=71428"

var "DLogLvl" "4"

# dbLoadRecords "evt-nep.db" "P=$(PSCE),EN=IonMagSt,CPRD=$(CPRD)"
# dbLoadRecords "evt.db" "P=$(PSCE),EN=LebtCpOn,CPRD=$(CPRD)"
# dbLoadRecords "evt.db" "P=$(PSCE),EN=LebtCpOff,CPRD=$(CPRD)"
# dbLoadRecords "evt.db" "P=$(PSCE),EN=RFSt,CPRD=$(CPRD)"
# dbLoadRecords "evt-lnk-sts.db" "P=$(PSCE),ENL=LebtCp,EN1=LebtCpOn,EN2=LebtCpOff"

# dbLoadRecords "evt.db"     "P=$(P),OBJ=Nep,Evt=EvtIonMagSt"
# dbLoadRecords "evt.db"     "P=$(P),OBJ=Nep,Evt=EvtIonMagEnd"
# dbLoadRecords "evt.db"     "P=$(P),Evt=EvtIonMagSt"
# dbLoadRecords "evt.db"     "P=$(P),Evt=EvtIonMagEnd"
# dbLoadRecords "evt.db"     "P=$(P),Evt=EvtLebtCpOn"
# dbLoadRecords "evt.db"     "P=$(P),Evt=EvtLebtCpOff"
# dbLoadRecords "evt.db"     "P=$(P),Evt=EvtRFSt"

#dbLoadRecords "evtlSts.db" "P=$(P),OBJ=Nep,Evtl=EvtlIonMag,Evt1=EvtIonMagSt,Evt2=EvtIonMagEnd"
#dbLoadRecords "evtlSts.db" "P=$(P),Evtl=EvtlIonMag,Evt1=EvtIonMagSt,Evt2=EvtIonMagEnd"
#dbLoadRecords "evtlSts.db" "P=$(P),Evtl=EvtlLebtCp,Evt1=EvtLebtCpOff,Evt2=EvtLebtCpOn"

# [ENV]
# iocshLoad "$(TOP)/test/iocsh/envEss.iocsh"
# # [Evts]
# iocshLoad "$(TOP)/test/iocsh/evtPkg.iocsh"     "P=$(PSCE):,Dir=$(TOP)/test/iocsh,File=evt.iocsh"

#iocshLoad "$(mrfioc2_DIR)/evtPkg.iocsh"     "P=$(PSCE):,Path=$(supercycleengine_DIR)/sceEvt.iocsh"

# Common

iocInit

dbl > /tmp/sce.pvlist
system "grep '[^_]$' /tmp/sce.pvlist | grep -v EVG | grep -v REQMOD | grep -v SC-IOC | sed -E 's/$(PSCE)//g' > $(TOP)/docs/sce.pvlist"
